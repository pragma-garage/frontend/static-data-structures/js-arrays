/**
 * Recorrer los elementos de un arreglo, uno a uno
 */
var runner = function () {
  // Recorrer un arreglo usando un ciclo u los índices
  let sampleArray = [3, 6, 1, 56, 8, 3, 4]
  console.log(
    'Teniendo en cuenta la forma en que se accede a los elementos de un arreglo, es posible recorrerlo, ',
    'sea para modificarlo o simplemente para mostrar uno a uno sus elementos. Si tenemos el arreglo:\n',
    sampleArray,
    '\n\nPodemos usar un ciclo for para recorrer cada uno de sus posiciones y mostrar cada uno de sus elementos:'
  )
  for (let i = 0; i < sampleArray.length; i++) {
    console.log(sampleArray[i])
  }
}