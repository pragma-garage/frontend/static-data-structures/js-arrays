/**
 * Este es el archivo maestro que ejecuta los ejemplos descritos. Evite modificar este archivo.
 */
console.log(
  '¡Hola!',
  '\n\nSi estas leyendo este mensaje es porque abriste correctamente el Inspector.',
  '\n\nPara ejecutar los archivos basta con interactuar con los enlaces y botones que ves en el navegador.'
);

function run(action) {
  let scripts = document.getElementsByTagName('script')
  let filteredScripts = [...scripts].filter(value => {
    return null !== value.src.match(new RegExp(`/js/samples/${action}.js`))
  })
  if (0 === filteredScripts.length) {
    console.log('')
    console.log(`Ejecutando ${action}`)
    let newScript = document.createElement('script')
    newScript.onload = function () {
      console.clear()
      runner()
    }
    newScript.src = `./js/samples/${action}.js`
    document.body.appendChild(newScript)
  } else {
    filteredScripts.forEach(value => value.remove())
    run(action)
  }
}

[...document.getElementsByTagName('li')].forEach(listElement => {
  [...listElement.getElementsByTagName('span')].forEach(spanElement => {
    if (undefined !== spanElement.dataset.name) {
      const executeButton = document.createElement('button')
      executeButton.type = 'button'
      executeButton.innerText = 'Ejecutar'
      executeButton.onclick = () => run(spanElement.dataset.name)
      listElement.appendChild(executeButton)
      const openCodeLink = document.createElement('a')
      openCodeLink.href = `./js/samples/${spanElement.dataset.name}.js`
      openCodeLink.target = '_blank'
      openCodeLink.innerText = 'Ver código'
      listElement.appendChild(openCodeLink)
    }
  })
})
